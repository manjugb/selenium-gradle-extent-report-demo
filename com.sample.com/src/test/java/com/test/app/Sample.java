/*
@author Manjunath
*/

package com.test.app;

import com.aventstack.extentreports.Status;
import common.TestBaseClass;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class Sample extends TestBaseClass {
	

    @Test(groups = "Smoke")
    public void tc00VerifyURL() {

        test = extent.createTest("Verify URL", "Test the mystore link")
                .assignCategory("Functional_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Verify URL");

        webdriver.openURL("http://automationpractice.com");
        test.log(Status.INFO, "Open URL");
        logger.info("Open URL");
    }

    @Test
    public void tc01VerifyEnterText() {

        test = extent.createTest("Verify Product Search Box", "")
                .assignCategory("Functional_TestCase")
                .assignCategory("Positive_TestCase")
                .assignAuthor("Manjunath");
        logger.info("Verify Search Box");

        webdriver.enterText(IConstants.enterProductKey, "Printed");
        webdriver.clickOnButton(IConstants.searchBut);
        String verifytext = webdriver.getText(IConstants.results);
        test.log(Status.INFO, "Verify Search Box");
        logger.info("Verify Search Box");
        if(verifytext.equalsIgnoreCase("5 results have been found.")) {
        assertEquals(verifytext, "5 results have been found.");
        test.log(Status.PASS, verifytext);}
        else {
        	assertEquals(verifytext, "2 results have been found.");
        	test.log(Status.FAIL, verifytext);}
        }
        
        
    
}
