# Gradle with Jenkins Integration #

## Preconditions ##
### Pre-Installations ###
#### Java ####
https://www.java.com/en/download/help/index_installing.html
#### Gradle ####
https://gradle.org/install/
#### Jenkins #####
https://www.jenkins.io/doc/book/installing/

## Project Setup in Jenkins ##
* Go to Dash board click on  new item -> name it -> choose free style option
* use custom workspare or integrate the bitbucket or github repository
